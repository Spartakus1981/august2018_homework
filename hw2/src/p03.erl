-module(p03).
-export([element_at/2]).

element_at([],_) ->
	{error, empty_list};

element_at([H|T], N) ->
	element_at([H|T], N, 1).

element_at([H|_T], N, N) ->
	H;

element_at([_H|T], N, Acc) ->
	element_at(T, N, Acc +1);
%	io:format("~p~n", [Acc]);

element_at(_, _, _) ->
	undefined.
