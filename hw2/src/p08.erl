-module(p08).
-export([compress/1]).

compress(List) ->
	compress(List,[]).

compress([], Acc) ->
	p05:reverse(Acc);

compress([[]|T], Acc) ->
	compress(T, Acc);

compress([AccH|T], [AccH|AccT]) ->
	compress(T, [AccH|AccT]);

compress([H|T], Acc) ->
	compress(T, [H|Acc]).
