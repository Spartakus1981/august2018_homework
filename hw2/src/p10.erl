-module(p10).
-export([encode/1]).

encode(List) ->
	encode(List, 1, [], []).


encode([H|[H|T]], N, _Acc1, Acc2) ->
	encode([H|T], N + 1, H, Acc2);

encode([H|T], 1, [], Acc2) ->
	encode(T, 1, [], [{1, H}|Acc2]);

encode([_H|T], N, Acc1, Acc2) ->
%	io:format("clause 3, Str 15~n H: ~p~n T: ~p~n Acc1: ~p~n Acc2: ~p~n~n", [H, T, Acc1, Acc2]),  
	encode(T, 1, [], [{N, Acc1}|Acc2]);

encode([], _N, _Acc1, Acc2) ->
	p05:reverse(Acc2).
