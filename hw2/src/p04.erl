-module(p04).
-export([len/1]).

len(List) ->
	len(List, 0).

len([], Acc) ->
	Acc;

len([_H|T], Acc) ->
	len(T, Acc + 1).
