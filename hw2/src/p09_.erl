-module(p09_).
-export([pack/1]).

pack(List) ->
	pack(List, []).


pack([H|T], [[H|_]=AccH|AccT]) ->
	io:format("str 9~n H: ~p~n T: ~p~n AccH: ~p~n AccT: ~p~n~n", [H, T, AccH, AccT]),
	pack(T, [[H|AccH]|AccT]);

pack([H|T], Acc) ->
	io:format("str 13~n H: ~p~n T: ~p~n Acc: ~p~n~n", [H, T, Acc]), 
	pack(T, [[H]|Acc]);

pack([], Acc) ->
	p05:reverse(Acc).

