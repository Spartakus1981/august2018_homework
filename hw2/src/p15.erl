-module(p15).
-export([replicate/2]).

replicate(List, N) ->
	replicate(List, N, 0, []).

replicate([_H|T], N, N, Acc) ->
	replicate(T, N, 0, Acc);

replicate([H|T], N, NN, Acc) ->
	replicate([H|T], N, NN+1, [H|Acc]);

replicate([], _N, _NN, Acc) ->
	p05:reverse(Acc). 


