-module(p07).
-export([flatten/1]).

flatten(List) ->
	p05:reverse(flatten(List, [])).

flatten([H|[HH|TT]], Acc) ->
	io:format("str 7 ~n H: ~p~n HH: ~p~n TT: ~p~n Acc: ~p~n~n", [H,HH,TT,Acc]),
	flatten(TT, flatten(HH, [H|Acc]));


%flatten([[]|T], Acc) ->
%	flatten(T, Acc);

%flatten([H|[]], Acc) ->
%	flatten([], [H|Acc]);

%flatten([[H]], Acc) ->
%	flatten(H, Acc);

%flatten([[H|T1]|T2], Acc) ->
%	flatten([T1], [H|Acc]),
%	io:format("~p~n", [T2]);

flatten([H|T], Acc) ->
	io:format("str 25 ~n H: ~p~n T: ~p~n Acc: ~p~n~n", [H,T,Acc]),
	flatten(H, Acc);

flatten([], Acc) ->
	Acc;


flatten(N, Acc) ->
	[N|Acc].
