-module(p01).
-export([last/1]).

last([]) ->
	{error, empty_list};

last([T|[]]) ->
	T;

last([_H|T]) ->
	last(T).
