-module(p09).
-export([pack/1]).

pack([ListH|ListT]) ->
	pack(ListT, [ListH], []).


pack([Acc1|T], [Acc1|Acc1T], Acc2) ->
	pack(T, [Acc1|[Acc1|Acc1T]], Acc2);

pack([H|T], Acc1, Acc2) ->
	pack(T, [H], [Acc1|Acc2]);

pack([], Acc1, Acc2) ->
	p05:reverse([Acc1|Acc2]).

