-module(p14_comprehension).
-export([duplicate/1]).


duplicate(List) ->
	lists:flatten([[N,N]|| N <- List]). 

