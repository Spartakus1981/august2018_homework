-module(p12).
-export([decode_modified/1]).

decode_modified(List) ->
	decode_modified(List, []).


decode_modified([{E1,E2}|T], Acc) ->
	io:format("str 8~n E1: ~p~n E2: ~p~n T: ~p~n Acc: ~p~n~n", [E1,E2,T,Acc]),
	decode_modified(T, acc(E1,E2)++Acc);

decode_modified([H|_]=[_H1|T], Acc) ->
	io:format("str 12~n H: ~p~n T: ~p~n Acc: ~p~n~n", [H,T,Acc]),
	decode_modified(T, [H|Acc]);

decode_modified([], Acc) ->
	p05:reverse(Acc).





acc(E1, E2) ->
	io:format("str 23: ~n E1: ~p~n E2: ~p~n~n", [E1,E2]),
	acc(E1, [E2], 1).

acc(E1, E2, E1) ->
	io:format("str 27: ~n E1: ~p~n E2: ~p~n~n", [E1,E2]),
	E2;

acc(E1, [EH|_]=[EH1|ET], EX) ->
	io:format("str 31: ~n E1: ~p~n E2: ~p~n ET: ~p~n EX: ~p~n~n", [E1,EH,ET,EX]),
	acc(E1, [EH|[EH1|ET]], EX+1).
