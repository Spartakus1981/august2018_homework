-module(p11).
-export([encode_modified/1]).

encode_modified(List) ->
	encode_modified(List, 1, [], []).


encode_modified([H|[H|T]], N, _Acc1, Acc2) ->
	encode_modified([H|T], N + 1, H, Acc2);

encode_modified([H|T], 1, [], Acc2) ->
	encode_modified(T, 1, [], [H|Acc2]);

encode_modified([_H|T], N, Acc1, Acc2) ->
%	io:format("clause 3, Str 15~n H: ~p~n T: ~p~n Acc1: ~p~n Acc2: ~p~n~n", [H, T, Acc1, Acc2]),  
	encode_modified(T, 1, [], [{N, Acc1}|Acc2]);

encode_modified([], _N, _Acc1, Acc2) ->
	p05:reverse(Acc2).
