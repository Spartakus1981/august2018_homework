-module(p14).
-export([duplicate/1]).

duplicate(List) ->
	duplicate(List, []).

duplicate([H|T], Acc) ->
%	io:format("Str 7 ~n H: ~p~n T: ~p~n TAcc: ~p~n~n", [H,T,Acc]),
	duplicate(T, [H,H|Acc]);

duplicate([], Acc) ->
	p05:reverse(Acc).
